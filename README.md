#   Descriptions
    Simple tools for check validate email address. Write on python code. Results in JSON format

##  Dependencies
    - Python 2.7.x
    - gunicorn
    - email-validator
    - validate-email
    - web.py
    
### How to use
    - simple : http://localhost:8000/emailvalidate/?email=my@domain.com
    - detail : http://localhost:8000/emailvalidate/?email=my@domain.com&detail=true
    