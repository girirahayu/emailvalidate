from validate_email import validate_email as check
from email_validator import validate_email as getinfo
import json
import web
import urllib
import smtplib
from datetime import datetime
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from lxml import html
import requests
import urllib2
import socket

class EmailValidate:
    def emailcheck(self,email,detail):

        is_valid = check(email,check_mx=True)
        if is_valid:
            if detail == "true":
                output = getinfo(email)
                return json.dumps(output, sort_keys=True, indent=2, separators=(',', ': '))
            else:
                output = {'email': email,
                          'valid': is_valid,
                          }
                web.header('Content-Type', 'Application/json')
                web.header('Mail-Support', 'infra@codigo.id')
                web.header('Powered-By', 'Web.py')
                return json.dumps(output, sort_keys=True, indent=2, separators=(',', ': '))


        else:
            output = {'email': email,
                      'valid': False,
                      }

            web.header('Content-Type', 'Application/json')
            web.header('Mail-Support', 'infra@codigo.id')
            web.header('Powered-By', 'Web.py')

            return json.dumps(output, sort_keys=True, indent=2, separators=(',', ': '))

class GetStatus:
    def cekdomain(self,domain):
        domain = str(domain)
        domainfile = domain.replace(".","")
        response = urllib2.urlopen('http://'+domain, timeout=10)

        try:
            if response.getcode() != 200:
                output = {'code': response.getcode(),
                          'status': "down",
                          }
                f = open("%s" %domainfile,"r")
                f = f.read()
                if f == "up":
                    i = datetime.now()
                    datenow = i.strftime('%Y-%m-%d %H:%M:%S')
                    fromaddr = "no-reply@codigo.id"
                    toaddr = "infra@codigo.id" # for multiple ['abc@com','bca@com']
                    msg = MIMEMultipart()
                    msg['From'] = fromaddr
                    msg['To'] = toaddr
                    msg['Subject'] = domain + " DOWN @ " + datenow

                    body = "@" + datenow
                    msg.attach(MIMEText(body, 'plain'))

                    try:
                        smtpObj = smtplib.SMTP('localhost')
                        text = msg.as_string()
                        smtpObj.sendmail(fromaddr, toaddr, text)
                        smtpObj.quit()

                        f = open("%s" %domainfile, "w")
                        f.write("down")
                        f.close()
                        print "sukses"

                    except Exception:
                        print "Error: unable to send email"

            else:
                output = {'code': response.getcode(),
                          'status': "up",
                          }
                try:
                    f = open("%s" %domainfile, "r")
                    f = f.read()
                    if f == "down":
                        i = datetime.now()
                        datenow = i.strftime('%Y-%m-%d %H:%M:%S')
                        fromaddr = "no-reply@codigo.id"
                        toaddr = "infra@codigo.id"
                        msg = MIMEMultipart()
                        msg['From'] = fromaddr
                        msg['To'] = toaddr
                        msg['Subject'] = domain + " UP @ " + datenow

                        body = "@" + datenow
                        msg.attach(MIMEText(body, 'plain'))

                        try:
                            smtpObj = smtplib.SMTP('localhost')
                            text = msg.as_string()
                            smtpObj.sendmail(fromaddr, toaddr, text)
                            smtpObj.quit()

                            f = open("%s" %domainfile, "w")
                            f.write("up")
                            f.close()
                            print "sukses"

                        except Exception:
                            print "Error: unable to send email"
                except Exception:

                    w = open("%s" %domainfile,"w")
                    w.write("up")
                    w.close()

        except urllib2.HTTPError:
            output = {'code': "unknow",
                      'status': "down",
                      }
            f = open("%s" % domainfile, "r")
            f = f.read()
            if f == "up":
                i = datetime.now()
                datenow = i.strftime('%Y-%m-%d %H:%M:%S')
                fromaddr = "no-reply@codigo.id"
                toaddr = "infra@codigo.id"  # for multiple ['abc@com','bca@com']
                msg = MIMEMultipart()
                msg['From'] = fromaddr
                msg['To'] = toaddr
                msg['Subject'] = domain + " Unknow Respone " + datenow + " Please Check "

                body = "@" + datenow
                msg.attach(MIMEText(body, 'plain'))

                try:
                    smtpObj = smtplib.SMTP('localhost')
                    text = msg.as_string()
                    smtpObj.sendmail(fromaddr, toaddr, text)
                    smtpObj.quit()

                    f = open("%s" % domainfile, "w")
                    f.write("down")
                    f.close()
                    print "sukses"

                except Exception:
                    print "Error: unable to send email"


        web.header('Content-Type', 'Application/json')
        web.header('Mail-Support', 'infra@codigo.id')
        web.header('Powered-By', 'Web.py')

        return json.dumps(output, sort_keys=True, indent=2, separators=(',', ': '))

class GetParseDglog:
    def getEmelementParse(self,page):
        p = requests.get(page)
        tree = html.fromstring(p.content)
        log = tree.xpath('//div[@id="content"]//li/text()')


        log = log[40] #get data list ke 20
        log = log.replace('\n',"")
        log = log.replace(' ',"")
        log = log.replace('logprocess:','')

        #log = int(log)

        domainfile = "dglogprocess"

        if log > 1000:
            f = open("%s" %domainfile,"r")
            f = f.read()
            if f == "up":
                i = datetime.now()
                datenow = i.strftime('%Y-%m-%d %H:%M:%S')
                fromaddr = "no-reply@codigo.id"
                toaddr = "infra@codigo.id" # for multiple ['abc@com','bca@com']
                msg = MIMEMultipart()
                msg['From'] = fromaddr
                msg['To'] = toaddr
                msg['Subject'] = "WARN Dglog Process"

                body = "Dglog Process has been upto "+ str(log) +" @ "+ datenow
                msg.attach(MIMEText(body, 'plain'))

                try:
                    smtpObj = smtplib.SMTP('localhost')
                    text = msg.as_string()
                    smtpObj.sendmail(fromaddr, toaddr, text)
                    smtpObj.quit()

                    f = open("%s" %domainfile, "w")
                    f.write("down")
                    f.close()
                    print "sukses"

                except Exception:
                    print "Error: unable to send email"
        else:
            try:
                f = open("%s" %domainfile, "r")
                f = f.read()
                if f == "down":
                    i = datetime.now()
                    datenow = i.strftime('%Y-%m-%d %H:%M:%S')
                    fromaddr = "no-reply@codigo.id"
                    toaddr = "infra@codigo.id"
                    msg = MIMEMultipart()
                    msg['From'] = fromaddr
                    msg['To'] = toaddr
                    msg['Subject'] = "Dglog Process Normal"

                    body = "Dglog Process Normal @ " + datenow
                    msg.attach(MIMEText(body, 'plain'))

                    try:
                        smtpObj = smtplib.SMTP('localhost')
                        text = msg.as_string()
                        smtpObj.sendmail(fromaddr, toaddr, text)
                        smtpObj.quit()

                        f = open("%s" %domainfile, "w")
                        f.write("up")
                        f.close()
                        print "sukses"

                    except Exception:
                        print "Error: unable to send email"
            except Exception:
                w = open("%s" %domainfile,"w")
                w.write("up")
                w.close()

        output = {'logprocess':log

        }
        web.header('Content-Type', 'Application/json')
        web.header('Mail-Support', 'infra@codigo.id')
        web.header('Powered-By', 'Web.py')
        return json.dumps(output, sort_keys=True, indent=2, separators=(',', ': '))



